# Lista di servizi di supporto psicologico in Italia su covid-19

Questa lista è aperta al contributo di chiunque voglia partecipare.

## Come segnalare un nuovo servizio

Se vuoi contribuire segnalando un nuovo servizio puoi procedere in tre modi.

### Aprire una issue su Gitlab

**Cosa serve?**

E' necessario un account su [Gitlab](https://gitlab.com/users/sign_up)

**Cosa devo fare?** 

Devi aprire una [nuova issue](https://gitlab.com/xho/supporto-psicologico-covid-19/-/issues/new) scrivendo nel campo **Title** il nome del servizio che vorresti aggiungere e completando la descrizione seguendo le indicazioni dell'esempio che troverai nella descrizione della issue stessa.

### Aprire una Merge Request su Gitlab

Se sei uno sviluppatore puoi contribuire aprendo una [Merge Request](https://gitlab.com/xho/supporto-psicologico-covid-19/-/merge_requests/new) integrando la lista dei servizi presenti nel file `static/list.json`.

### Segnala su Twitter

**Cosa serve?**

E' necessario un account su Twitter.

**Cosa devo fare?**

Devi mandare un messaggio diretto a [@xho](https://twitter.com/xho) o a [@batopa](https://twitter.com/batopa) indicando nel messaggio le informazioni o meglio un link relativo al servizio.

__

### Dov'è il sito?

A questo indirizzo:
**https://xho.gitlab.io/supporto-psicologico-covid-19/**
